FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

RUN set -x && \
    apt-get update && \
    apt-get install -y --no-install-recommends ruby-full ruby-bundler build-essential curl && \
    rm -rf /var/lib/apt/lists/*

COPY Gemfile Gemfile.lock /
RUN bundle install --system && \
    rm Gemfile Gemfile.lock

RUN adduser --disabled-login --gecos "" gitlab
USER gitlab
WORKDIR /home/gitlab
